<?php
session_start();
if($_SESSION['user_role'] <> 1)
{
	header('Location: '.$_SERVER['SERVER_NAME'].'/Exit.php');
}

$_POST = json_decode(file_get_contents('php://input'), true);

if(isset($_GET['page'])){
    getData($_GET['page'],$_GET['num_rows']);
}

if($_GET['is_name_uniq']){
    getIsNameUniq($_GET['is_name_uniq']);
}

if(isset($_POST['add_kultur'])){
    addKultur($_POST['add_kultur']);
}

if($_POST['edit_kultur']){
    editKultur($_POST['edit_kultur']);
}


function getData($page,$num_rows){
    //$dbc = null;
    //if($dbc==null){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    //}
    // echo json_encode(['error'=>$dbc]);
    // return;

    $query = "SELECT * FROM `Kultura` LIMIT ?,?";
    
    // $query = "SELECT t_k.`id`, 
    //     t_k.`name` as name_kultur,
    //     t_kck.`id_class_kultur`,
    //     t_ck.`name` as name_class_kultur
    //     FROM `Kultura` t_k
    //     left join `Kultura_Class_Kultur` t_kck on (t_k.`id` = t_kck.`id_kultura`)
    //     left join `Class_Kultur` t_ck on (t_ck.`id` = t_kck.`id_class_kultur`)
    //     ORDER BY t_k.`name`
    //     LIMIT ?,?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    // if ( !$stmt ) {
    //     echo 'mysqli error: '.mysqli_error($dbc);
    //     return;
    // }
    mysqli_stmt_bind_param($stmt, 'ii', $p ,$t_num_rows);
    $p = $page;
    if($page>0){
        $p= $page*$num_rows;
    }
    $t_num_rows= $num_rows+1;
    $rows = [];
    mysqli_stmt_execute($stmt);
    
        /* Определить переменные для результата */
    mysqli_stmt_bind_result($stmt, $id, $name_kultur);
    
        /* Выбрать значения */
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = [
            'id'=>$id,
            'name_kultur'=>$name_kultur,
            'arr_class_kultur'=>getArrClassCultur($id)
        ];
            //echo $name;
            //printf ("%s (%s)\n", $id, $name);
    }
        //print_r($rows);
        /* Завершить запрос */
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>['data'=>$rows,'num_rows'=>count($rows)]]);
}

function getArrClassCultur($id){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    $query = "SELECT t_ck.`id` as 'id_class_kultur',
        t_ck.`name` as 'name_class_kultur',
        t_kck.`id` as 'id_kult_class_kult' 
        FROM `Kultura_Class_Kultur` t_kck 
        LEFT JOIN `Class_Kultur` t_ck on(t_ck.`id`=t_kck.`id_class_kultur`) 
        WHERE t_kck.`id_kultura`=?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);

    mysqli_stmt_execute($stmt);
    $rows = [];
        /* Определить переменные для результата */
    mysqli_stmt_bind_result($stmt, $id_class_kultur, $name_class_kultur, $id_kult_class_kult);
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = [
            'id_class_kultur'=>$id_class_kultur,
            'name_class_kultur'=>$name_class_kultur,
            'id_kult_class_kult'=>$id_kult_class_kult
        ];
    }

    return $rows;
}

function addKultur($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    // echo json_encode(['error'=>$data]);
    // return;

    if(!$data['name']){
        //если надо массивы обьекты переводим в json строку
        echo json_encode(['error'=>'Культуре необходимо дать Имя']);
        //return что бы прекратить дальнейшее выполнение скрипта
        return;
    }

    if(!isNameUniq($data['name'])){
        echo json_encode(['error'=>'Данное название Культуры занято']);
        return;
    }
    
    $query = "INSERT INTO `Kultura`(`name`) VALUES (?)";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 's', $name);
    $name = $data['name'];
    
    mysqli_stmt_execute($stmt);
    $id_kultura = mysqli_insert_id($dbc);
    mysqli_stmt_close($stmt);

    $i = count($data['arr_class_kultur']);
    if($i>0){
        $unknown_num_vals = '';
        $params = [];
        $typs = '';
        foreach ($data['arr_class_kultur'] as $value){
            $unknown_num_vals.='(?,?)';
            $params[] = $id_kultura;
            $params[] = $value;
            $typs.='ii';
            if($i!=1){
                $unknown_num_vals.=', ';
            }
            $i--;
        }
    }else{
        $unknown_num_vals = '(?,?)';
        $params = [$id_kultura,1];
        $typs = 'ii';
    }
    
    $query = "INSERT INTO `Kultura_Class_Kultur`(`id_kultura`,`id_class_kultur`) VALUES ".$unknown_num_vals;

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, $typs, ...$params);

    if(!mysqli_stmt_execute($stmt)){
        echo json_encode(['error'=>'добавить Классы Культур не удалось']);
        mysqli_stmt_close($stmt);
        return;
    }

    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>'Добавил Культуру']);
    return;
}

function editKultur($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    // echo json_encode(['error'=>$data]);
    // return;

    //"{id: 1, name_kultur: "Пшениц", arr_class_kultur: Array(1)}"
    if(!$data['name_kultur']){
        //если надо массивы обьекты переводим в json строку
        echo json_encode(['error'=>'Культуре необходимо дать Имя']);
        //return что бы прекратить дальнейшее выполнение скрипта
        return;
    }

    if(iconv_strlen($data['name_kultur'])<2){
        echo json_encode(['error'=>'Слишком короткое Название']);
        //return что бы прекратить дальнейшее выполнение скрипта
        return;
    }

    $query = "SELECT `name` FROM `Kultura` WHERE `name`=? AND `id`!=?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'si', $name ,$id);

    $name = $data['name_kultur'];
    $id=$data['id'];

    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $t_name);

    while (mysqli_stmt_fetch($stmt)) {
        if($t_name){
            echo json_encode(['error'=>'Название Занято']);
            //return что бы прекратить дальнейшее выполнение скрипта
            return;
        }
    }

    mysqli_stmt_close($stmt);

    $query = "UPDATE `Kultura` SET `name` = ? WHERE id = ?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'si', $name ,$id);
    $name = $data['name_kultur'];
    $id=$data['id'];
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    if(count($data['arr_class_kultur'])<=0){
        echo json_encode(['success'=>"Обновили Культуру"]);
        return;
    }
    updateClassKultur($id,$data['arr_class_kultur']);
}

function updateClassKultur($id, $arr){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    
    $query = "SELECT `id_class_kultur` 
        FROM `Kultura_Class_Kultur`
        WHERE `id_kultura`=?";
    
    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'i' ,$id);

    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $id_class_kultur);
    
    //$old_arr = [];
    /* Выбрать значения */
    while (mysqli_stmt_fetch($stmt)) {
        for($i=0;$i<count($arr);$i++){
            if($arr[$i]===$id_class_kultur){
                array_splice($arr,$i,1);
                $i--;
            }
        }
    }

    mysqli_stmt_close($stmt);

    $i = count($arr);
    if($i<=0){
        echo json_encode(['success'=>'Обновили Культуру']);
        return;
    }

    $unknown_num_vals = '';
    $params = [];
    $typs = '';
    
    foreach ($arr as $value){
        $unknown_num_vals.='(?,?)';
        $params[] = $id;
        $params[] = $value;
        $typs.='ii';
        if($i!=1){
            $unknown_num_vals.=', ';
        }
        $i--;
    }

    $query = "INSERT INTO `Kultura_Class_Kultur`(`id_kultura`,`id_class_kultur`) VALUES ".$unknown_num_vals;

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, $typs, ...$params);

    if(!mysqli_stmt_execute($stmt)){
        echo json_encode(['error'=>'добавить Классы не удалось']);
        mysqli_stmt_close($stmt);
        return;
    }

    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>'Обновили Культуру']);
    return;
}

function getIsNameUniq($name){
    if(isNameUniq($name)){
        echo json_encode(['is_uniq'=>true]);
        return;
    }
    echo json_encode(['is_uniq'=>false]);
    return;
}

function isNameUniq($name){
    if($dbc==null){
        include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    }
    $query = "SELECT `id` FROM `Kultura` WHERE `name`=?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return false;
    }

    mysqli_stmt_bind_param($stmt, 's', $name);

    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $t_id);
    
    while (mysqli_stmt_fetch($stmt)) {
        return false;
    }

    mysqli_stmt_close($stmt);

    return true;
}

?>