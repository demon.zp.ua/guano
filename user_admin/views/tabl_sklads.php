<?php
// require_once './user_admin/getInfoUser.php';
require_once 'form_edit_sklad.php';
require_once 'form_add_sklad.php';

session_start();
if($_SESSION['user_role'] <> 1)
{
    header('Location: '.$_SERVER['DOCUMENT_ROOT'].'/Exit.php');
}

function getSkladTable(){
    //<a class="btn btn-success" role="button" id="add_comp"></a>
    //<img src="..." class="rounded mr-2" alt="...">
    getSkladEditForm();
    getSkladAddForm();
    echo <<<EOD
    <div id="component-sklads">
    <div class="btn-group-vertical">
        <button type="button" class="btn btn-success" id="add_sklad">Добавить Склад</button>
        <button type="button" class="btn btn-secondary btn-lg" disabled></button>
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-secondary btn-sm" name="previous">Предыдущая</button>
            <button type="button" class="btn btn-secondary btn-sm" disabled>| страница |</button>
            <button type="button" class="btn btn-secondary btn-sm" name="next">Следующая</button>
        </div>
    </div>
<table class="table table-striped" id="sklad-table">
    <thead>
        <tr>
            <th>#</th>
            <th>название склада</th>
            <th>название компании</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row" name="id"></th>
            <th name="name_sklad"></th>
            <th name="name_company"></th>
            <th name="action">
                <button type="button" class="btn btn-primary" name="edit">Редактировать</button>
                <button type="button" class="btn btn-danger" name="del">Удалить</button>
            </th>
        </tr>
    </tbody>
</table>
</div>
<script src="js/user_admin/tab_sklad.js"></script>
EOD;
}
?>