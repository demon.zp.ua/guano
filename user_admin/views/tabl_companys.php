<?php
//require_once './user_admin/getInfoUser.php';
require_once 'form_edit_company.php';
require_once 'form_add_company.php';

session_start();
if($_SESSION['user_role'] <> 1)
{
    header('Location: '.$_SERVER['DOCUMENT_ROOT'].'/Exit.php');
}

function getCompanyTable(){
    //<a class="btn btn-success" role="button" id="add_comp"></a>
    //<img src="..." class="rounded mr-2" alt="...">
    
    getCompnyEditForm();
    getCompnyAddForm();
    echo <<<EOD
    <div id="component-form-add-company">
    <div class="btn-group-vertical">
        <button type="button" class="btn btn-success" id="add_comp">Добавить Компанию</button>
        <button type="button" class="btn btn-secondary btn-lg" disabled></button>
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-secondary btn-sm" name="previous">Предыдущая</button>
            <button type="button" class="btn btn-secondary btn-sm" disabled>| страница |</button>
            <button type="button" class="btn btn-secondary btn-sm" name="next">Следующая</button>
        </div>
    </div>
<table class="table table-striped" id="comp-skald-table">
    <thead>
        <tr>
            <th>#</th>
            <th>название компании</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row" name="id"></th>
            <th name="name"></th>
            <th name="action">
                <button type="button" class="btn btn-primary" name="edit">Редактировать</button>
                <button type="button" class="btn btn-danger" name="del">Удалить</button>
            </th>
        </tr>
    </tbody>
</table>
</div>
<script src="js/user_admin/tab_company.js"></script>
EOD;
}
?>

