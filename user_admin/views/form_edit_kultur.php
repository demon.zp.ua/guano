<?php

function getKulturEditForm(){
    echo <<<EOD
    
    <div class="modal fade" id="form-edit-kultur" tabindex="-1" role="dialog" aria-labelledby="..." aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="form-edit-kultur-title">Изменить Культуру</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger" role="alert" style="display:none;" name="error">
                <p></p>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Название</span>
                </div>
                <input type="text" class="form-control" name="name" aria-describedby="basic-addon3">
            </div>
            
            <div class="card mb-3" name="old_class_kultur">
                <div class="card-body">
                    This is some text within a card body.
                </div>
            </div>
            <div class="input-group-append" name="div_add_select">
                <button class="btn btn-success" type="button">Добавить Класс Культуры</button>
            </div>
            <div class="input-group mb-3" name="class_kultur">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="sklad_comp">Выберите Клас Культуры</label>
                </div>
                <select class="custom-select" name="class_kultur">
                </select>
                <div class="input-group-append">
                    <button class="btn btn-danger" type="button">Убрать</button>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary" name="save">Сохранить</button>
        </div>
        </div>
    </div>
    </div>
<script src="js/user_admin/form_edit_kultur.js"></script>
EOD;
}
?>