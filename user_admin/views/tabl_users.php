<?php
require_once 'form_add_user.php';
require_once 'form_edit_user.php';

session_start();
if($_SESSION['user_role'] <> 1)
{
    header('Location: '.$_SERVER['DOCUMENT_ROOT'].'/Exit.php');
}

function getUserTable(){

    getUserAddForm();
    getUserEditForm();
    echo <<<EOD
<div id="component-users">
    <div class="btn-group-vertical">
        <button type="button" class="btn btn-success" id="add_user">Добавить Пользователя</button>
        <button type="button" class="btn btn-secondary btn-lg" disabled></button>
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-secondary btn-sm" name="previous">Предыдущая</button>
            <button type="button" class="btn btn-secondary btn-sm" disabled>| страница |</button>
            <button type="button" class="btn btn-secondary btn-sm" name="next">Следующая</button>
        </div>
    </div>
    <table class="table table-striped" id="user-table">
    <thead>
        <tr>
        <th>#</th>
        <th>Имя пользователя</th>
        <th>Роль</th>
        <th>Склады(Компания)</th>
        <th>Активен</th>
        <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row" name="id"></th>
            <th name="name_user"></th>
            <th name="name_role"></th>
            <th name="sklad_comp"></th>
            <th name="is_activ"></th>
            <th name="action">
                <button type="button" class="btn btn-primary" name="edit">Редактировать</button>
            </th>
        </tr>
        </tbody>
</table>
</div>
<script src="js/user_admin/tab_user.js"></script>
EOD;
}    
?>