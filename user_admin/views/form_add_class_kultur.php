<?php

session_start();
if($_SESSION['user_role'] <> 1)
{
    header('Location: '.$_SERVER['DOCUMENT_ROOT'].'/Exit.php');
}

function getClassKulturAddForm(){
    echo <<<EOD
    
    <div class="modal fade" id="form-add-class-kultur" tabindex="-1" role="dialog" aria-labelledby="form-add-class-kultur-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="form-add-class-kultur-title">Добавить Класс Культур</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Название</span>
                </div>
                <input type="text" class="form-control" name="name" aria-describedby="basic-addon3">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary" name="save">Сохранить</button>
        </div>
        </div>
    </div>
    </div>
<script src="js/user_admin/form_add_class_kultur.js"></script>
EOD;
}
?>