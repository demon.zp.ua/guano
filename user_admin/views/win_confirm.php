<?php

function getWinConfirm(){
    echo <<<EOD
    
    <div class="modal fade" id="win-confirm" tabindex="-1" role="dialog" aria-labelledby="form-add-company-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="win-confirm-title">Подтвердите действие</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary" name="confirm">Подтвердить</button>
        </div>
        </div>
    </div>
    </div>
<script src="js/user_admin/win_confirm.js"></script>
EOD;
}
?>