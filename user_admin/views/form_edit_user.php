<?php

function getUserEditForm(){
    echo <<<EOD
    
    <div class="modal fade" id="form-edit-user" tabindex="-1" role="dialog" aria-labelledby="..." aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="form-edit-user-title">Изменить Пользователя</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger" role="alert" style="display:none;" name="error">
                <p></p>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Логин</span>
                </div>
                <input type="text" class="form-control" name="name" aria-describedby="basic-addon3">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Пароль</span>
                </div>
                <input type="password" class="form-control" name="password" aria-describedby="basic-addon3">
            </div>
            <div class="input-group mb-3">
                <input class="form-control" type="text" placeholder="Readonly input here..." readonly name="role">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="is_activ">Выберите активен ли пользователь</label>
                </div>
                <select class="custom-select" name="is_activ">
                    <option value="1">Активен</option>
                    <option value="0">Не активен</option>
                </select>
            </div>
            <div class="card mb-3" name="old_sklad_comp">
                <div class="card-body">
                    This is some text within a card body.
                </div>
            </div>
            <div class="input-group-append" name="div_add_select">
                <button class="btn btn-success" type="button">Добавить Склад(Компания)</button>
            </div>
            <div class="input-group mb-3" name="sklad_comp">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="sklad_comp">Выберите Склад</label>
                </div>
                <select class="custom-select" name="sklad_comp">
                </select>
                <div class="input-group-append">
                    <button class="btn btn-danger" type="button">Убрать</button>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary" name="save">Сохранить</button>
        </div>
        </div>
    </div>
    </div>
<script src="js/user_admin/form_edit_user.js"></script>
EOD;
}
?>