<?php
require_once 'form_edit_kultur.php';
require_once 'form_add_kultur.php';

session_start();
if($_SESSION['user_role'] <> 1)
{
    header('Location: '.$_SERVER['DOCUMENT_ROOT'].'/Exit.php');
}

function getKulturTable(){
    //<a class="btn btn-success" role="button" id="add_comp"></a>
    //<img src="..." class="rounded mr-2" alt="...">
    getKulturEditForm();
    getKulturAddForm();
    echo <<<EOD
    <div id="component-kultur">
    <div class="btn-group-vertical">
        <button type="button" class="btn btn-success" id="add_kultur">Добавить Культуру</button>
        <button type="button" class="btn btn-secondary btn-lg" disabled></button>
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-secondary btn-sm" name="previous">Предыдущая</button>
            <button type="button" class="btn btn-secondary btn-sm" disabled>| страница |</button>
            <button type="button" class="btn btn-secondary btn-sm" name="next">Следующая</button>
        </div>
    </div>
<table class="table table-striped" id="kultur-table">
    <thead>
        <tr>
            <th>#</th>
            <th>название Культуры</th>
            <th>Классы Культуры</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row" name="id"></th>
            <th name="name_kultur"></th>
            <th name="arr_class_kultur"></th>
            <th name="action">
                <button type="button" class="btn btn-primary" name="edit">Редактировать</button>
            </th>
        </tr>
    </tbody>
</table>
</div>
<script src="js/user_admin/tab_kultur.js"></script>
EOD;
}
?>