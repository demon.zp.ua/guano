<?php

function getSkladAddForm(){
    echo <<<EOD
    
    <div class="modal fade" id="form-add-sklad" tabindex="-1" role="dialog" aria-labelledby="form-add-sklad-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="form-add-sklad-title">Добавить Склад</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Название</span>
                </div>
                <input type="text" class="form-control" name="name" aria-describedby="basic-addon3">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="user_role">Выберите компанию</label>
                </div>
                <select class="custom-select" name="select_company">
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary" name="save">Сохранить</button>
        </div>
        </div>
    </div>
    </div>
<script src="js/user_admin/form_add_sklad.js"></script>
EOD;
}
?>