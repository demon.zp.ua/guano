<?php

$_POST = json_decode(file_get_contents('php://input'), true);

if(isset($_GET['page'])){
    getData($_GET['page'],$_GET['num_rows']);
}

if(isset($_GET['get_comp'])){
    getComp();
}

if(isset($_POST['edit_sklad'])){
    updateSklad($_POST['edit_sklad']);
}

if(isset($_POST['add_sklad'])){
    addSklad($_POST['add_sklad']);
}

if(isset($_POST['del_sklad'])){
    deleteSklad($_POST['del_sklad']['id']);
}

function getData($page,$num_rows){
    //$dbc = null;
    //if($dbc==null){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    //}
    // echo json_encode(['error'=>$dbc]);
    // return;
    
    $query = "SELECT t_s.`id`, 
        t_s.`name` as name_sklad,
        t_cs.`id_company`,
        t_c.`name` as name_company
        FROM `Sklad` t_s
        left join `Comp_Sklad` t_cs on (t_s.id = t_cs.id_sklad)
        left join `Company` t_c on (t_c.id = t_cs.id_company)
        ORDER BY t_c.`name`
        LIMIT ?,?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    // if ( !$stmt ) {
    //     echo 'mysqli error: '.mysqli_error($dbc);
    //     return;
    // }
    mysqli_stmt_bind_param($stmt, 'ii', $p ,$t_num_rows);
    $p = $page;
    if($page>0){
        $p= $page*$num_rows;
    }
    $t_num_rows= $num_rows+1;
    $rows = [];
    mysqli_stmt_execute($stmt);
    
        /* Определить переменные для результата */
    mysqli_stmt_bind_result($stmt, $id_sklad, $name_sklad,$id_company,$name_company);
    
        /* Выбрать значения */
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = ['id'=>$id_sklad,'name_sklad'=>$name_sklad,'id_company'=>$id_company,'name_company'=>$name_company];
            //echo $name;
            //printf ("%s (%s)\n", $id, $name);
    }
        //print_r($rows);
        /* Завершить запрос */
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>['data'=>$rows,'num_rows'=>count($rows)]]);
}

function getComp(){

//    Отображение строк 0 - 5 (6 всего, Запрос занял 0.0530 сек.)
//SELECT t_c.`id`,t_c.`name`,t_cs.`id_company` FROM `Company` t_c left join `Comp_Sklad` t_cs on(t_c.`id`=t_cs.`id_company`) WHERE t_cs.`id_company` is NULL
//select * from `Company` where `Company`.`id` not in (select `id_company` from `Comp_Sklad`)
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
        
    $query = "SELECT * FROM `Company`";
    
        if(!$stmt = mysqli_prepare($dbc, $query)){
            echo json_encode(['error'=>mysqli_error($dbc)]);
            return;
        }
    
        // if ( !$stmt ) {
        //     echo 'mysqli error: '.mysqli_error($dbc);
        //     return;
        // }
        $rows = [];
        mysqli_stmt_execute($stmt);
        
            /* Определить переменные для результата */
        mysqli_stmt_bind_result($stmt, $id, $name);
        
            /* Выбрать значения */
        while (mysqli_stmt_fetch($stmt)) {
            $rows[] = ['id'=>$id,'name'=>$name];
                //echo $name;
                //printf ("%s (%s)\n", $id, $name);
        }
            //print_r($rows);
            /* Завершить запрос */
        mysqli_stmt_close($stmt);
    
        echo json_encode(['success'=>['data'=>$rows]]);
}

function updateSklad($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    //sleep(10);

    $query = "UPDATE `Sklad` SET `name` = ? WHERE id = ?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'si', $name ,$id);
    $name = $data['name'];
    $id=$data['id'];
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>'Все пучком']);
    return;
}

function addSklad($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    
    if(empty($data['name'])){
        echo json_encode(['error'=>'Введите название компании']);
        return;
    }

    $query = "INSERT INTO `Sklad`(`name`) VALUES (?)";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 's', $name);
    $name = $data['name'];
    
    mysqli_stmt_execute($stmt);
    $id_sklad = mysqli_insert_id($dbc);
    mysqli_stmt_close($stmt);

    $query = "INSERT INTO `Comp_Sklad`(`id_company`,`id_sklad`) VALUES (?,?)";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'ii', $id_comp, $t_id_sklad);
    $t_id_sklad = $id_sklad;
    $id_comp = $data['id_comp'];

    mysqli_stmt_execute($stmt);
    $id_c_s = mysqli_insert_id($dbc);
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>$id_c_s]);
    return;

}

function deleteSklad($id){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    
    $query = "DELETE FROM `Comp_Sklad` WHERE `id_sklad` = ?";
    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'i', $t_id);
    $t_id = $id;
    
    if(!mysqli_stmt_execute($stmt)){
        echo json_encode(['error'=>'Удалить запись не удалось, возможно она связана с другими данными']);
        mysqli_stmt_close($stmt);
        return;
    }

    mysqli_stmt_close($stmt);

    $query = "DELETE FROM `Sklad` WHERE `id` = ?";
    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'i', $t_id);
    $t_id = $id;
    
    if(mysqli_stmt_execute($stmt)){
        echo json_encode(['success'=>$id]);
    }else{
        echo json_encode(['error'=>'Удалить запись не удалось, возможно она связана с другими данными']);
    }

    mysqli_stmt_close($stmt);

    //echo json_encode(['success'=>$company_id]);
    //return;
}
?>