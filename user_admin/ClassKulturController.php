<?php
session_start();
if($_SESSION['user_role'] <> 1)
{
	header('Location: '.$_SERVER['SERVER_NAME'].'/Exit.php');
}

$_POST = json_decode(file_get_contents('php://input'), true);

if(isset($_GET['page'])){
    getData($_GET['page'],$_GET['num_rows']);
}

if(isset($_POST['add_class_kultur'])){
    addClassKultur($_POST['add_class_kultur']);
}

if(isset($_POST['edit_class_kultur'])){
    updateClassKultur($_POST['edit_class_kultur']);
}

if(isset($_POST['del_class_kultur'])){
    deleteClassKultur($_POST['del_class_kultur']['id']);
}

function getData($page,$num_rows){
    //$dbc = null;
    //if($dbc==null){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    //}
    // echo json_encode(['error'=>$dbc]);
    // return;
    
    $query = "SELECT `id`, `name` FROM `Class_Kultur` LIMIT ?,?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    // if ( !$stmt ) {
    //     echo 'mysqli error: '.mysqli_error($dbc);
    //     return;
    // }
    mysqli_stmt_bind_param($stmt, 'ii', $p ,$t_num_rows);
    $p = $page;
    if($page>0){
        $p= $page*$num_rows;
    }
    $t_num_rows= $num_rows+1;
    $rows = [];
    mysqli_stmt_execute($stmt);
    
        /* Определить переменные для результата */
    mysqli_stmt_bind_result($stmt, $id, $name);
    
        /* Выбрать значения */
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = ['id'=>$id,'name'=>$name];
            //echo $name;
            //printf ("%s (%s)\n", $id, $name);
    }
        //print_r($rows);
        /* Завершить запрос */
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>['data'=>$rows,'num_rows'=>count($rows)]]);
}

function addClassKultur($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    
    if(empty($data['name'])){
        echo json_encode(['error'=>'Введите название компании']);
        return;
    }

    $query = "INSERT INTO `Class_Kultur`(`name`) VALUES (?)";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 's', $name);
    $name = $data['name'];
    
    mysqli_stmt_execute($stmt);
    $class_kultur_id = mysqli_insert_id($dbc);
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>$class_kultur_id]);
    return;

}

function updateClassKultur($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    //sleep(10);

    $query = "UPDATE `Class_Kultur` SET `name` = ? WHERE id = ?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'si', $name ,$id);
    $name = $data['name'];
    $id=$data['id'];
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>'Все пучком']);
    return;
}

function deleteClassKultur($id){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    
    $query = "DELETE FROM `Class_Kultur` WHERE `id` = ?";
    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    
    if(!mysqli_stmt_execute($stmt)){
        echo json_encode(['error'=>'Удалить запись не удалось, возможно она связана с другими данными']);
        mysqli_stmt_close($stmt);
        return;
    }

    echo json_encode(['success'=>$id]);
}
?>