<?php
// echo $_SERVER['SERVER_NAME'];
// return;
session_start();
if($_SESSION['user_role'] <> 1)
{
	header('Location: '.$_SERVER['SERVER_NAME'].'/Exit.php');
}
//getElem();
//include $_SERVER['DOCUMENT_ROOT'].'/user_admin/CompanySkladController.php';
//вот так можно вытащить данные отосланые из axios Пост запросом
$_POST = json_decode(file_get_contents('php://input'), true);

//-----какието обработчики на то что делать при том или ином запросе
if($_POST['edit_user']){
    editUser($_POST['edit_user']);
}

if(isset($_POST['add_user'])){
    addUser($_POST['add_user']);
}

if(isset($_GET['page'])){
    getUsers($_GET['page'],$_GET['num_rows']);
}

if($_GET['is_name_uniq']){
    getIsNameUniq($_GET['is_name_uniq']);
}

if($_GET['get_comp_sklad']){
    getNoUserSkladComp();
}

function getUsers($page,$num_rows){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    // echo json_encode(['error'=>$num_rows]);
    // return;
    $query = "SELECT t_u.`id`,
        t_u.`name` as 'name_user',
        t_u.`is_activ`,
        t_ur.`id` as 'id_ur',
        t_r.`id` as 'id_r',
        t_r.`name` as 'name_role' FROM `User` t_u 
        LEFT JOIN `Users_Roles` t_ur on(t_ur.`id_user`=t_u.`id`)
        LEFT JOIN `Roles` t_r on(t_r.`id`=t_ur.`id_roles`)
        ORDER BY t_u.`is_activ` DESC,t_r.`name`
        LIMIT ?,?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    mysqli_stmt_bind_param($stmt, 'ii', $p ,$t_num_rows);
    $p = $page;
    if($page>0){
        $p= $page*$num_rows;
    }
    $t_num_rows= $num_rows+1;
    $rows = [];
    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $id, $name_user,$is_activ,$id_user_role,$id_role,$name_role);
    
        /* Выбрать значения */
    //$i = 0;
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = [
            'id'=>$id,
            'name_user'=>$name_user,
            'is_activ'=>$is_activ,
            'id_user_role'=>$id_user_role,
            'id_role'=>$id_role,
            'name_role'=>$name_role,
            'sklad_comp'=>getSkladComp($id_user_role,$id_role)
        ];
            //echo $name;
            //printf ("%s (%s)\n", $id, $name);
    }
        //print_r($rows);
        /* Завершить запрос */
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>['data'=>$rows,'num_rows'=>count($rows)]]);

}

function getSkladComp($id,$id_role){
    if($id_role<3){
        return [];
    }
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    $query = "SELECT 
        t_cs.`id`,
        t_c.`name` as 'name_comp',
        t_s.`name` as 'name_sklad' 
        FROM `Users_Sklad` t_us
        LEFT JOIN `Comp_Sklad` t_cs on(t_us.`Id_Comp_Sklad`=t_cs.`id`)
        LEFT JOIN `Company` t_c on(t_c.`id`=t_cs.`id_company`)
        LEFT JOIN `Sklad` t_s on(t_s.`id`=t_cs.`id_sklad`)
        WHERE t_us.`id_users_roles`=?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        //echo json_encode(['error'=>mysqli_error($dbc)]);
        return ['error'=>mysqli_error($dbc)];
    }

    mysqli_stmt_bind_param($stmt, 'i', $t_id);
    $t_id = $id;
    
    $rows = [];

    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $id, $name_comp,$name_sklad);
    
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = [
            'id'=>$id,
            'name_comp'=>$name_comp,
            'name_sklad'=>$name_sklad
        ];
    }
    
    mysqli_stmt_close($stmt);

    return $rows;
}

function addUser($data){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    // echo json_encode(['error'=>$data]);
    // return;

    if(!$data['name']){
        //если надо массивы обьекты переводим в json строку
        echo json_encode(['error'=>'Пользователю необходимо дать Имя']);
        //return что бы прекратить дальнейшее выполнение скрипта
        return;
    }

    if(!$data['password']){
        echo json_encode(['error'=>'Пользователю необходимо дать Пароль']);
        return;
    }

    if(!is_int($data['role'])){
        echo json_encode(['error'=>'Пользователю необходимо дать Роль']);
        return;
    }

    if($data['role']==3){
        if(count($data['arr_comp_sklad'])<=0){
            echo json_encode(['error'=>'Оператор долежн иметь Компанию-Склад']);
            return;
        }else{
            $arr_id_comp_sklad = $data['arr_comp_sklad'];
        }
    }

    if(!isNameUniq($t_data['name'])){
        echo json_encode(['error'=>'Данное Имя занято']);
        return;
    }

    $t_data['name'] = $name = $data['name'];
    $t_data['pass'] = $data['password'];
    $t_data['hash'] = 'SHA('.$t_data['pass'].')';
    $t_data['is_activ'] = 1;
    $id_role = $data['role'];
    // echo json_encode(['text'=>'Все Пучком!!']);
    // return;

    $i = count($t_data);
    $cols = '';
    $vals = '';
    foreach($t_data as $key=>$value){
        if($key!=='hash'){
            $vals.="'" . $value . "' ";
        }else{
            $vals.= $value;
        }

        $cols.= "`".$key."`";
            
        if($i!=1){
            $vals.= ', ';
            $cols.= ", ";
		}
        $i--;
    }
    
    $vals.=");";
    $query = "INSERT INTO `User`(".$cols.") VALUES (".$vals;

    //echo json_encode(['error'=>$query]);
    //return;
    $user = mysqli_query($dbc, $query);
    //var_dump($response);
    //return;
    if($user !== true){
        echo json_encode(['error'=>'ошибка добавления Юзера']);
        return;
    }
    
    $user_id = mysqli_insert_id($dbc);
    // echo json_encode(['error'=>'создали Юзера = '.$user_id]);
    // return;
    // else{
    //     echo json_encode(['error'=>'создали Юзера = '.mysqli_insert_id($dbc)]);
    //     return;
    // }

    $query = "INSERT INTO `Users_Roles`(`id_user`,`id_roles`) VALUES (".$user_id.", ".$id_role.")";
    //echo json_encode(['error'=>$query]);
    //return;
    $user_role = mysqli_query($dbc, $query);

    if($user_role !== true){
        echo json_encode(['error'=>'ошибка добавления Роли']);
        return;
    }
    
    // echo json_encode(['error'=>'создали Роли = '.mysqli_insert_id($dbc)]);
    // return;

    if($id_role<3){
        echo json_encode(['success'=>'Отлично создали Юзера']);
        return;
    }

    $id_user_role = mysqli_insert_id($dbc);
    //$id_user_role = 2;

    $i = count($arr_id_comp_sklad);
    $vals = '';
    foreach($arr_id_comp_sklad as $key=>$value){
        $vals.="('".$value."','" .$id_user_role. "')";
         
        if($i!=1){
            $vals.= ', ';
		}
        $i--;
    }
    
    $vals.=";";
    //INSERT INTO `Users_Sklad` (`Id`, `Id_Comp_Sklad`, `Id_Users_Roles`) VALUES (NULL, '10', '2'), (NULL, '11', '2');
    $query = "INSERT INTO `Users_Sklad`(`id_comp_sklad`,`id_users_roles`) VALUES".$vals;
    // echo json_encode(['error'=>$query]);
    // return;
    $role_comp = mysqli_query($dbc, $query);
    if($role_comp !== true){
        echo json_encode(['error'=>'ошибка добавления Компани-Склада']);
        return;
    }
    echo json_encode(['success'=>'Отлично создали Юзера']);
}

function editUser($data){
    //echo json_encode(['error'=>$data]);
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    if(!$data['name_user']){
        //если надо массивы обьекты переводим в json строку
        echo json_encode(['error'=>'Пользователю необходимо дать Имя']);
        //return что бы прекратить дальнейшее выполнение скрипта
        return;
    }

    if(strlen($data['password'])>0 && strlen($data['password'])<3){
        echo json_encode(['error'=>'Пароль не может быть меньше 3 символов']);
        return;
    }

    $t_data['name'] = $name = $data['name_user'];
    $id = $data['id'];
    //echo json_encode(['$data_dop'=>$dbc]);
    //return;
    $query = "SELECT * FROM `User` WHERE `name`='$name' and `id`!='$id';";
    //echo $query;
    //return;
    $response = mysqli_query($dbc, $query);
    //var_dump($response);
    //return;
    if(mysqli_num_rows($response)!=0){
        echo json_encode(['error'=>'Пользователь с таким Именем уже есть']);
        return;
    }

    $query = "UPDATE `User` SET ";
    //echo '$data[is_activ] = '.$data['is_activ'];
    //return;
    //if($data['is_activ']=='1' && $data['is_activ']=='0'){
    $t_data['is_activ'] = $data['is_activ'];
    //}else{
    //    $t_data['is_activ'] = 1;
    //}

    if(strlen($data['password'])>0){
        $t_data['pass'] = $data['password'];
        $t_data['hash'] = 'SHA('.$t_data['pass'].')';
    }
    //var_dump($data_dop);
    //$t_data['hash'] = ;
    $i = count($t_data);
    $set = '';
    foreach($t_data as $key=>$value){
        //if($value){
            //echo 'добавляю = '.$key." = ".$value;
            if($key!=='hash'){
                $set.="`" . $key . "` = '" . $value . "' ";
            }else{
                $set.="`" . $key . "` = " . $value . " ";
            }
            
            if($i!=1){
				$set.= ', ';
			}
        //}
        $i--;
    }

    $query.=$set." WHERE id=".$id.";";
    //$stmt = $dbc->prepare("UPDATE `User` SET (?) WHERE id = (?)")
    //$query = "UPDATE `User` SET (?) WHERE id = (?)"
    //echo $query;
    //return;
    $user = mysqli_query($dbc, $query);
    if($user !== true){
        echo json_encode(['error'=>'ошибка обновления Юзера']);
        return;
    }

    if($data['id_role']!=3){
        echo json_encode(['success'=>'Обновили Пользователя']);
        return;
    }

    if($data['is_activ']==0){
        $result = toTryRemoveSklads($data['id_user_role']);
        if(array_key_exists('error',$result)){
            echo json_encode(['error'=>'Статус изменен, но при отвязке складов ошибка: '.$result['error']]);
            return;
        }else{
            echo json_encode(['success'=>['msg'=>'Обновили Пользователя','dop_msg'=>$result['success']]]);
            return;
        }
    }

    if(count($data['arr_comp_sklad'])<=0){
        echo json_encode(['success'=>'Обновили Пользователя']);
        return;
    }

    $unknown_num_vals = '';
    $params = [];
    $typs = '';
    $i = count($data['arr_comp_sklad']);
    foreach ($data['arr_comp_sklad'] as $value){
        $unknown_num_vals.='(?,?)';
        $params[] = $value;
        $params[] = $data['id_user_role'];
        $typs.='ii';
        if($i!=1){
            $unknown_num_vals.=', ';
        }
        $i--;
    }

    $query = "INSERT INTO `Users_Sklad`(`id_comp_sklad`,`id_users_roles`) VALUES ".$unknown_num_vals;

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return;
    }

    // echo json_encode(['success'=>'подготовил))))']);
    // return;

    mysqli_stmt_bind_param($stmt, $typs, ...$params);

    if(!mysqli_stmt_execute($stmt)){
        echo json_encode(['error'=>'добавить Склады не удалось']);
        mysqli_stmt_close($stmt);
        return;
    }

    mysqli_stmt_close($stmt);
    echo json_encode(['success'=>'Обновили Пользователя']);
    return;
}

function toTryRemoveSklads($id){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    $query = "SELECT `id`,`id_comp_sklad` FROM `Users_Sklad` WHERE `id_users_roles`=?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        return ['error'=>mysqli_error($dbc)];
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);

    if(!mysqli_stmt_execute($stmt)){
        return ['error'=>'Узнать склады Юзера не удалось'];
    }

    mysqli_stmt_bind_result($stmt, $id, $id_comp_sklad);

    $rows = [];

    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = ['id'=>$id,'id_comp_sklad'=>$id_comp_sklad];
    }

    mysqli_stmt_close($stmt);

    $not_del_sklads = [];
    $del_sklads = [];

    foreach ($rows as $value) {
        $query = "DELETE FROM `Users_Sklad` WHERE `id` = ?";

        if(!$stmt = mysqli_prepare($dbc, $query)){
            return ['error'=>'в foreach: '.mysqli_error($dbc)];
        }

        mysqli_stmt_bind_param($stmt, 'i', $value['id']);

        if(!mysqli_stmt_execute($stmt)){
            $not_del_sklads[]=$value['id_comp_sklad'];
            mysqli_stmt_close($stmt);
        }else{
            $del_sklads[]=$value['id_comp_sklad'];
            mysqli_stmt_close($stmt);
        }
    }

    return ['success'=>['not_del_sklads'=>$not_del_sklads,'del_sklads'=>$del_sklads]];
}

function getNoUserSkladComp(){
    include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';

    $query = "SELECT t_cs.`id`,t_c.`name` as 'name_comp',t_s.`name` as 'name_sklad' FROM `Comp_Sklad` t_cs
        LEFT JOIN `Users_Sklad` t_us on(t_us.`id_comp_sklad`=t_cs.`id`)
        LEFT JOIN `Company` t_c on (t_c.`id` = t_cs.`id_company`)
        LEFT JOIN `Sklad` t_s on(t_s.`id`=t_cs.`id_sklad`)
        WHERE t_us.`id_comp_sklad` is NULL
        ORDER BY t_c.`name`";
    
    if(!$stmt = mysqli_prepare($dbc, $query)){
        //echo json_encode(['error'=>mysqli_error($dbc)]);
        return ['error'=>mysqli_error($dbc)];
    }

    $rows = [];

    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $id,$name_comp,$name_sklad);
    
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = [
            'id'=>$id,
            'name_comp'=>$name_comp,
            'name_sklad'=>$name_sklad
        ];
    }
    
    mysqli_stmt_close($stmt);

    echo json_encode(['success'=>['data'=>$rows]]);

}

function getIsNameUniq($name){
    if(isNameUniq($name)){
        echo json_encode(['is_uniq'=>true]);
        return;
    }
    echo json_encode(['is_uniq'=>false]);
    return;
}

function isNameUniq($name){
    if($dbc==null){
        include $_SERVER['DOCUMENT_ROOT'].'/Connect.php';
    }
    $query = "SELECT `id` FROM `User` WHERE `name`=?";

    if(!$stmt = mysqli_prepare($dbc, $query)){
        echo json_encode(['error'=>mysqli_error($dbc)]);
        return false;
    }

    mysqli_stmt_bind_param($stmt, 's', $name);

    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $t_id);
    
    while (mysqli_stmt_fetch($stmt)) {
        return false;
    }

    mysqli_stmt_close($stmt);

    return true;
}

?>