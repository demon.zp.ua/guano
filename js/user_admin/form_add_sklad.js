class FormAddSklad{
    constructor(){
        this.main = document.getElementById('form-add-sklad');
        this.content = this.main.querySelector('div[class$=modal-body]');
        this.el_name = this.content.querySelector('input[name$="name"]');
        this.el_select_company = this.content.querySelector('select[name$="select_company"]');
        this.arr_comp = '';

        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name$=save]').addEventListener('click', this.onSave.bind(this));
    }

    render(event){
        this.clearSelect();
        console.log('должон открыть форму адд скад');
        this.arr_comp = event.detail.arr_comp;
        this.arr_comp.forEach(comp => {
            let newOption = new Option(comp.name, comp.id);
            this.el_select_company.append(newOption);
        });
        $('#form-add-sklad').modal('show');
    }

    onSave(event){
        if(this.el_name.value.length<2){
            console.log("Название не может быть короче 2 символов");
            return;
        }
        $('#form-add-sklad').modal('hide');
        let main = document.getElementById('sklad-table');
        main.dispatchEvent(new CustomEvent("addRow", {
            detail: {
                name: this.el_name.value,
                id_comp: this.el_select_company.value,
                name_comp: this.el_select_company.options[this.el_select_company.selectedIndex].text
            }
        }));
        this.el_name.value = '';
    }

    clearSelect(){
        const collect_option = this.el_select_company.querySelectorAll('option');
        for (let option of collect_option) {
            option.remove();
        }
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new FormAddSklad();