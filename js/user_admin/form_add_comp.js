class FormAddCompany{
    constructor(){
        this.main = document.getElementById('form-add-company');
        this.content = this.main.querySelector('div[class$=modal-body]');
        this.el_name = this.content.querySelector('input[name$=name]');
        this.temp_name = '';

        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name$=save]').addEventListener('click', this.onSave.bind(this));
    }

    render(event){
        $('#form-add-company').modal('show');
    }

    onSave(event){
        if(this.el_name.value.length<2){
            console.log("Название не может быть короче 2 символов");
            return;
        }
        $('#form-add-company').modal('hide');
        let main = document.getElementById('comp-skald-table');
        main.dispatchEvent(new CustomEvent("addRow", {
            detail: {
                name: this.el_name.value
            }
        }));
        this.el_name.value = '';
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new FormAddCompany();