class ClassKultur{
    constructor(){
        this.main = document.getElementById('class-kultur-table');
        this.btn_add = document.getElementById('add_class_kultur');
        this.btn_previous = document.getElementById('component-form-add-class-kultur').querySelector('button[name="previous"]');
        this.btn_next = document.getElementById('component-form-add-class-kultur').querySelector('button[name="next"]');

        this.btn_previous.addEventListener('click', this.previousPage.bind(this));
        this.btn_next.addEventListener('click',this.nextPage.bind(this));
        this.btn_add.addEventListener('click', this.onClickAdd.bind(this));
        this.main.addEventListener('render', this.loadData.bind(this));
        this.main.addEventListener('delete', this.delete.bind(this));
        this.main.addEventListener('updateRow', this.onSave.bind(this));
        this.main.addEventListener('addRow', this.addRow.bind(this));
        this.main.addEventListener('delClassKultur', this.delClassKultur.bind(this));

        this.el_templete_row = null;
        this.fon_load = null;
        this.data_table = {};
        this.page = 0;
        this.num_rows = 20;
        this.has_page = false;
        this.id_del = null;
        this.getTempete();
    }

    getTempete(){
        this.fon_load = document.createElement('div');
        this.fon_load.style.position = 'absolute';
        this.fon_load.style.top = '0px';
        this.fon_load.style.width= window.innerWidth+'px';
        this.fon_load.style.height=document.documentElement.scrollHeight+'px';
        this.fon_load.style.margin= '0 auto';
        //this.fon_load.style.overflow= "hidden";
        this.fon_load.style.backgroundColor = '#000000';
        this.fon_load.style.opacity = 0.5;

        let el_t_row = this.main.querySelector('tbody').querySelector('tr');
        this.el_templete_row = el_t_row.cloneNode(true);
        //console.log('el_t_row = ', this.el_templete_row);
        el_t_row.remove();
    }

    createRow(row){
        this.data_table[row.id] = row;
        let el_temp_row = this.el_templete_row.cloneNode(true);
        el_temp_row.setAttribute('name', row.id);
        //console.log('row = ', el_temp_row);
        for(let column in row){
            let el_th = el_temp_row.querySelector(`th[name="${column}"]`);
            el_th.innerText = row[column];
            
            //console.log('el_th = ', el_th);
        }

        let btn_edit = el_temp_row.querySelector(`button[name="edit"]`);
        btn_edit.addEventListener('click', this.onClickEdit.bind(this));
        let btn_del = el_temp_row.querySelector(`button[name="del"]`);
        btn_del.addEventListener('click', this.onClickDel.bind(this));

        this.main.querySelector('tbody').append(el_temp_row);

    }

    updateRow(data){
        //const data = event.detail;
        this.data_table[data.id] = data;
        let el_temp_row = this.main.querySelector('tbody').querySelector(`tr[name="${data.id}"]`);
        el_temp_row.querySelector('th[name="name"]').innerText = data.name;
        //console.log("data = ", String(data.id).length);
        // if(String(data.id).length>=1 && String(data.name).length>=2){
        //     this.onSave(data);
        // }
    }

    loadData(){
        axios({
            method: 'get',
            url: '/user_admin/ClassKulturController.php',
            params:{
                page: this.page,
                num_rows: this.num_rows
            }
        })
        .then((response)=>{
            //console.log('response = ', response);
            if('error' in response.data){
                console.log('response = ', response.data.error);
                //this.addError([{text:response.data.error}]);
                return;
            }
            // for(let i=0;i<response.data.success.data.length-1;i++){
            //     this.createRow(response.data.success.data[i]);
            // }
            response.data.success.data.forEach((element,i) => {
                if(i<this.num_rows){
                    this.createRow(element);
                }
            });
            if(this.num_rows<response.data.success.num_rows){
                this.has_page = true;
                this.showBtnNext(true);
            }else{
                this.has_page = false;
                this.showBtnNext(false);
            }
            // for(let row in response.data.success.data){
                
            // }
            
        })
        .catch((error)=>{
            console.log('error = ', error);
        });
    }

    onClickEdit(event){
        let id = event.target.parentElement.parentElement.getAttribute('name');
        document.getElementById('form-edit-class-kultur').dispatchEvent(new CustomEvent("render", {
            detail: { 
                id: this.data_table[id].id,
                name: this.data_table[id].name
            }
        }));
        
        //console.log('должен редактировать = ', event.target.parentElement.parentElement.getAttribute('name'));
    }

    onClickAdd(){
        document.getElementById('form-add-class-kultur').dispatchEvent(new CustomEvent("render"));
    }

    onClickDel(event){
        //console.log('тут!!!');
        let id = event.target.parentElement.parentElement.getAttribute('name');
        this.id_del = id;
        document.getElementById('win-confirm').dispatchEvent(new CustomEvent("render", {
            detail: {
                text:`Вы действительно хотите удалить Класс-Культуры "${this.data_table[id].name}"?`,
                callback:'delClassKultur',
                component:this.main
            }
        }));
    }

    addRow(event){
        const data = event.detail;
        if(String(data.name).length<2){
            return;
        }
        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/ClassKulturController.php',
            data:{
                add_class_kultur:{
                    name: String(data.name)
                }
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            //console.log('response = ', response);
            this.fon_load.remove();
            this.delete();
            this.loadData();
            alert(`Класс "${data.name}" успешно добавлен`);
            //document.body.setAttribute('class','');
            //resolve(response.data);
            return;
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //reject(error);
        });
    }

    onSave(event){
        const data = event.detail;
        if(String(data.id).length<=1 && String(data.name).length<=2){
            return;
        }

        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/ClassKulturController.php',
            data:{
                edit_class_kultur:{
                    id: Number(data.id),
                    name: data.name
                }
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            this.updateRow(data);
            //console.log('response = ', response);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //resolve(response.data);
            return;
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //reject(error);
        });
    }

    delClassKultur(){
        //console.log('здесь');
        //return;
        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/ClassKulturController.php',
            data:{
                del_class_kultur:{
                    id: Number(this.id_del)
                }
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            if(response.data.hasOwnProperty('error')){
                alert(response.data.error);
                this.fon_load.remove();
                return;
            }
            if(response.data.hasOwnProperty('success')){
                alert(`Класс-Культуры "${this.data_table[response.data.success].name}" успешно удален`);
                this.delete();
                this.loadData();
                this.fon_load.remove();
                return;
            }
            console.log('error: ', response);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //resolve(response.data);
            
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //reject(error);
        });
    }

    previousPage(){
        if(this.page<=0){
            return;
        }
        this.page--;
        this.delete();
        this.loadData();
    }

    nextPage(){
        if(!this.has_page){
            return;
        }
        this.page++;
        this.delete();
        this.loadData();
    }

    showBtnNext(show){
        if(show){
            this.btn_next.style.display = '';
        }else{
            this.btn_next.style.display = 'none';
        }
    }

    delete(){
        this.data_table = {};
        //console.log(this.main.querySelector('tbody').rows);
        for(let elem of this.main.querySelector('tbody').querySelectorAll('tr')){
            elem.remove();
        }
    }
}

new ClassKultur();