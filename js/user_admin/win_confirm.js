class WinConfirm{
    constructor(){
        this.main = document.getElementById('win-confirm');
        this.el_text = this.main.querySelector('div[class$=modal-body]').firstElementChild;
        this.callback = null;
        this.component = null;
        this.text = null;
        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name$=confirm]').addEventListener('click', this.onConfirm.bind(this));
    }

    render(event){
        this.text = event.detail.text;
        this.el_text.innerText = event.detail.text;
        this.callback = event.detail.callback;
        this.component = event.detail.component;
        $('#win-confirm').modal('show');
    }

    onConfirm(event){
        //console.log('this.component = ', this.callback);
        $('#win-confirm').modal('hide');
        this.component.dispatchEvent(new CustomEvent(this.callback));
        //this.text = '';
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new WinConfirm();