class FormAddKultur{
    constructor(){
        this.main = document.getElementById('form-add-kultur');
        this.content = this.main.querySelector('div[class=modal-body]');
        this.el_name = this.content.querySelector('input[name="name"]');
        this.el_error = this.content.querySelector('div[name="error"]');
        
        this.templete_select_class_kultur = null;
        this.div_add_select = this.content.querySelector('div[name="div_add_select"]');

        this.arr_sklad_comp = null;
        this.arr_el_select = [];
        this.temp_name = '';
        this.is_name_uniq = false;

        this.el_name.addEventListener('blur', this.isNameUniq.bind(this));
        this.div_add_select.firstElementChild.addEventListener('click', this.addSelectCompSklad.bind(this));
        // this.el_role.addEventListener('change', this.onSelectRole.bind(this));
        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name=save]').addEventListener('click', this.onSave.bind(this));
        //this.getTempete();
    }

    setTempete(arr_class_kultur){
        if(this.templete_select_class_kultur){
            let select = this.templete_select_class_kultur.querySelector('select');
            select.innerHTML = '';
            
            arr_class_kultur.forEach(el=>{
                let newOption = new Option(`${el.name}`, el.id);
                select.append(newOption);
            });
            return;
        }
        let div_class_kultur = this.content.querySelector('div[name="class_kultur"]');
        
        arr_class_kultur.forEach(el=>{
            let newOption = new Option(`${el.name}`, el.id);
            div_class_kultur.querySelector('select').append(newOption);
        });

        this.templete_select_class_kultur = div_class_kultur.cloneNode(true);

        div_class_kultur.remove();
    }

    render(event){
        this.el_name.value = '';
        this.delError();
        this.setTempete(event.detail.arr_class_kultur);
        this.dellSelectAll();
        //this.clearSelect();
        //console.log('должон открыть форму адд User');
        $('#form-add-kultur').modal('show');
    }

    setError(errors){
        let text = '';
        errors.forEach(error=>{
            text+=error.text+'<br>';
        });
        this.el_error.firstElementChild.innerHTML = text;
        this.el_error.style.display = '';
    }

    delError(){
        this.el_error.style.display = 'none';
    }

    createSelect(){
        let div_select = this.templete_select_class_kultur.cloneNode(true);
        let select = div_select.querySelector('select');
        select.setAt

        div_select.querySelector('button').addEventListener('click', this.dellSelect.bind(this));
        select.addEventListener('change', this.selectSklad.bind(this));
        this.arr_el_select.push(div_select);
        this.content.append(div_select);
    }

    showBtnAddSelect(show){
        if(show){
            this.div_add_select.style.display = '';
        }else{
            this.div_add_select.style.display = 'none';
        }
    }

    addSelectCompSklad(){
        let collect_div_select = this.content.querySelectorAll('select[name="class_kultur"]');
        for (let select of collect_div_select) {
            if(Number(select.value)===1){
                return;
            }
        }
        this.createSelect();
    }

    dellSelectAll(){
        //console.log('должен удалить все селекты');
        let collect_div_select = this.content.querySelectorAll('select[name="class_kultur"]');
        for (let select of collect_div_select) {
            select.parentElement.remove();
        }
    }

    dellSelect(event){
        event.target.parentElement.parentElement.remove();
        //console.log('Жмакнул удалить селект');
    }

    selectSklad(event){
        const coll_selets = this.content.querySelectorAll('select[name="class_kultur"]');
        
        //console.log('coll_selets!!! = ', coll_selets);
        for (let select of coll_selets) {
            if(select!==event.target){
                //console.log('select.value = ', select.value,'//',event.target.value);
                if(select.value===event.target.value){
                    event.target.value = 1;
                    return;
                }
            }
        }
    }

    onSelectRole(event){
        const el = event.target;
        if(el.value==='3'){
            //console.log('тут!!!');
            this.showBtnAddSelect(true);
            this.createSelect();
        }else{
            //console.log('должен удалить все селекты');
            this.dellSelectAll();
            this.showBtnAddSelect(false);
        }
    }

    isNameUniq(event){
        
        if(this.el_name.value.length<2){
            this.setError([{text:'Слишком короткий Название'}]);
            return;
        }

        if(this.temp_name===this.el_name.value){
            return;
        }

        this.delError();

        axios({
            method: 'get',
            url: '/user_admin/KulturController.php',
            params:{
                is_name_uniq: this.el_name.value
            }
        })
        .then((response)=>{
            if('error' in response.data){
                //console.log('response = ', response);
                this.setError([{text:response.data.error}]);
                return;
            }else{
                if(response.data.is_uniq===false){
                    this.temp_name = this.el_name.value;
                    this.is_name_uniq = false;
                    this.setError([{text:'Такая Культура уже есть'}]);
                    return;
                }
                //console.log('response = ', response);
                this.is_name_uniq = true;
                this.temp_name = this.el_name.value;
                this.delError();
            }
        })
        .catch((error)=>{
            console.error('error = ', error);
        });
    }

    onSave(event){
        if(this.el_name.value.length<2){
            this.setError([{text:"Название не может быть меньше 1 символов"}]);
            //console.log("Логин не может быть меньше 3 символов");
            return;
        }

        if(!this.is_name_uniq){
            this.setError([{text:"Введите уникальный Название"}]);
            //console.log("Логин не может быть меньше 3 символов");
            return;
        }

        let arr_class_kultur = [];

        const coll_selets = this.content.querySelectorAll('select[name="class_kultur"]');
            //console.log('coll_selets!!! = ', coll_selets);
        for (let select of coll_selets) {
            if(Number(select.value)!==1){
                arr_class_kultur.push(Number(select.value));
            }
        }

        $('#form-add-kultur').modal('hide');
        let main = document.getElementById('kultur-table');
        main.dispatchEvent(new CustomEvent("addRow", {
            detail: {
                name: String(this.el_name.value),
                arr_class_kultur: arr_class_kultur
            }
        }));
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new FormAddKultur();