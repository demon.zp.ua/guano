class Main{
    constructor(){
        this.tab_comp = document.getElementById('tab-comp');
        this.tab_user = document.getElementById('tab-user');
        this.tab_sklad = document.getElementById('tab-sklad');
        this.tab_class_kultur = document.getElementById('tab-class-kultur');
        this.tab_kultur = document.getElementById('tab-kultur');
        //this.el_tab_comp = document.getElementById('comp-skald-table');

        this.tab_comp.addEventListener('click', this.onClickTabComp.bind(this));
        this.tab_user.addEventListener('click', this.onClickTabUser.bind(this));
        this.tab_sklad.addEventListener('click', this.onClickTabSklad.bind(this));
        this.tab_class_kultur.addEventListener('click', this.onClickTabClassKultur.bind(this));
        this.tab_kultur.addEventListener('click', this.onClickTabKultur.bind(this));

        let e = new Event("click", {bubbles: true});
        this.tab_user.dispatchEvent(e);
        this.tab_name = 'User';
    }

    onClickTabComp(event){
        if(this.tab_name===event.target.innerText){
            return;
        }
        this.delTabsContext();
        this.tab_name = event.target.innerText;
        let e = new Event("render", {bubbles: true});
        document.getElementById('comp-skald-table').dispatchEvent(e);
        //this.delTabsContext();
        //console.log('event = ', event.target.innerText);
    }

    onClickTabUser(event){
        if(this.tab_name===event.target.innerText){
            return;
        }
        this.delTabsContext();
        this.tab_name = event.target.innerText;
        let e = new Event("render", {bubbles: true});
        document.getElementById('user-table').dispatchEvent(e);
    }

    onClickTabSklad(event){
        if(this.tab_name===event.target.innerText){
            return;
        }
        //console.log('должен создать событие рендер skald-table');
        this.delTabsContext();
        this.tab_name = event.target.innerText;
        let e = new Event("render", {bubbles: true});
        document.getElementById('sklad-table').dispatchEvent(e);
    }

    onClickTabClassKultur(event){
        if(this.tab_name===event.target.innerText){
            return;
        }
        //console.log('должен создать событие рендер skald-table');
        this.delTabsContext();
        this.tab_name = event.target.innerText;
        let e = new Event("render", {bubbles: true});
        document.getElementById('class-kultur-table').dispatchEvent(e);
    }

    onClickTabKultur(event){
        if(this.tab_name===event.target.innerText){
            return;
        }
        //console.log('должен создать событие рендер skald-table');
        this.delTabsContext();
        this.tab_name = event.target.innerText;
        let e = new Event("render", {bubbles: true});
        document.getElementById('kultur-table').dispatchEvent(e);
    }

    delTabsContext(){
        let e = new Event("delete", {bubbles: true});
        document.getElementById('comp-skald-table').dispatchEvent(e);
        document.getElementById('sklad-table').dispatchEvent(e);
        document.getElementById('user-table').dispatchEvent(e);
        document.getElementById('class-kultur-table').dispatchEvent(e);
        document.getElementById('kultur-table').dispatchEvent(e);
    }
}

window.onload = function() {
    new Main();
};