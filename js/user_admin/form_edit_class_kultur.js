class FormEditClassKultur{
    constructor(){
        this.main = document.getElementById('form-edit-class-kultur');
        this.content = this.main.querySelector('div[class=modal-body]');
        this.el_name = this.content.querySelector('input[name=name]');
        this.el_id = this.content.querySelector('input[name=id]');
        this.id = 0;
        this.temp_name = '';

        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name=save]').addEventListener('click', this.onSave.bind(this));
    }

    render(event){
        const data = event.detail;
        this.id = data.id;
        this.el_id.setAttribute('placeholder', '#'+this.id);
        this.el_name.value = data.name;
        //document.body.setAttribute('class','modal-open');
        // this.main.setAttribute('modal', 'show');
        // this.main.setAttribute('aria-modal', 'true');
        // this.main.style.display = 'block';
        // this.main.removeAttribute('aria-hidden');
        $('#form-edit-class-kultur').modal('show');
        //this.main.setAttribute('data-modal', 'false');
        //$('#form-edit-company').keyboard('false');
    }

    onSave(event){
        $('#form-edit-class-kultur').modal('hide');
        let main = document.getElementById('class-kultur-table');
        main.dispatchEvent(new CustomEvent("updateRow", {
            detail: { 
                id: this.id,
                name: this.el_name.value
            }
        }));
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new FormEditClassKultur();