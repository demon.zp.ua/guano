class FormAddUser{
    constructor(){
        this.main = document.getElementById('form-add-user');
        this.content = this.main.querySelector('div[class=modal-body]');
        this.el_name = this.content.querySelector('input[name="name"]');
        this.el_password = this.content.querySelector('input[name="password"]');
        this.el_role = this.content.querySelector('select[name="role"]');
        this.el_error = this.content.querySelector('div[name="error"]');
        
        this.templete_select_sklad_comp = null;
        this.div_add_select = this.content.querySelector('div[name="div_add_select"]');

        this.arr_sklad_comp = null;
        this.arr_el_select = [];
        this.counter = 0;
        this.temp_name = '';
        this.is_name_uniq = false;

        this.el_name.addEventListener('blur', this.isNameUniq.bind(this));
        this.div_add_select.firstElementChild.addEventListener('click', this.addSelectCompSklad.bind(this));
        this.el_role.addEventListener('change', this.onSelectRole.bind(this));
        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name$=save]').addEventListener('click', this.onSave.bind(this));
        //this.getTempete();
    }

    setTempete(arr_sklad_comp){
        if(this.templete_select_sklad_comp){
            let select = this.templete_select_sklad_comp.querySelector('select');
            select.innerHTML = '';
            let defaultOption = new Option('Выберите Склад(Компанию)');
            select.append(defaultOption);
            arr_sklad_comp.forEach(comp_sklad=>{
                let newOption = new Option(`${comp_sklad.name_sklad}(${comp_sklad.name_comp})`, comp_sklad.id);
                select.append(newOption);
            });
            return;
        }
        let div_sklad_comp = this.content.querySelector('div[name$="sklad_comp"]');
        let defaultOption = new Option('Выберите Склад(Компанию)');
        div_sklad_comp.querySelector('select').append(defaultOption);
        arr_sklad_comp.forEach(comp_sklad=>{
            let newOption = new Option(`${comp_sklad.name_sklad}(${comp_sklad.name_comp})`, comp_sklad.id);
            div_sklad_comp.querySelector('select').append(newOption);
        });

        this.templete_select_sklad_comp = div_sklad_comp.cloneNode(true);

        div_sklad_comp.remove();
    }

    render(event){
        this.el_name.value = '';
        this.el_password.value = '';
        this.delError();
        this.setTempete(event.detail.arr_sklad_comp);
        this.dellSelectAll();
        //this.clearSelect();
        this.showBtnAddSelect(false);
        this.el_role.value = 'Выберите...';
        //console.log('должон открыть форму адд User');
        $('#form-add-user').modal('show');
    }

    setError(errors){
        let text = '';
        errors.forEach(error=>{
            text+=error.text+'<br>';
        });
        this.el_error.firstElementChild.innerHTML = text;
        this.el_error.style.display = '';
    }

    delError(){
        this.el_error.style.display = 'none';
    }

    createSelect(){
        this.counter++;
        let div_select = this.templete_select_sklad_comp.cloneNode(true);
        let select = div_select.querySelector('select');
        select.setAt

        div_select.querySelector('button').addEventListener('click', this.dellSelect.bind(this));
        select.addEventListener('change', this.selectSklad.bind(this));
        this.arr_el_select.push(div_select);
        this.content.append(div_select);
    }

    showBtnAddSelect(show){
        if(show){
            this.div_add_select.style.display = '';
        }else{
            this.div_add_select.style.display = 'none';
        }
    }

    addSelectCompSklad(){
        let collect_div_select = this.content.querySelectorAll('select[name$="sklad_comp"]');
        for (let select of collect_div_select) {
            if(select.value==='Выберите Склад(Компанию)'){
                return;
            }
        }
        this.createSelect();
    }

    dellSelectAll(){
        //console.log('должен удалить все селекты');
        let collect_div_select = this.content.querySelectorAll('select[name$="sklad_comp"]');
        for (let select of collect_div_select) {
            select.parentElement.remove();
        }
    }

    dellSelect(event){
        event.target.parentElement.parentElement.remove();
        //console.log('Жмакнул удалить селект');
    }

    selectSklad(event){
        const coll_selets = this.content.querySelectorAll('select[name$="sklad_comp"]');
        
        //console.log('coll_selets!!! = ', coll_selets);
        for (let select of coll_selets) {
            if(select!==event.target){
                //console.log('select.value = ', select.value,'//',event.target.value);
                if(select.value===event.target.value){
                    event.target.value = 'Выберите Склад(Компанию)';
                    return;
                }
            }
        }
    }

    onSelectRole(event){
        const el = event.target;
        if(el.value==='3'){
            //console.log('тут!!!');
            this.showBtnAddSelect(true);
            this.createSelect();
        }else{
            //console.log('должен удалить все селекты');
            this.dellSelectAll();
            this.showBtnAddSelect(false);
        }
    }

    isNameUniq(event){
        if(this.el_name.value.length<3){
            this.setError([{text:'Слишком короткий Логин'}]);
            return;
        }

        if(this.temp_name===this.el_name.value){
            return;
        }

        this.delError();

        axios({
            method: 'get',
            url: '/user_admin/UserController.php',
            params:{
                is_name_uniq: this.el_name.value
            }
        })
        .then((response)=>{
            if('error' in response.data){
                //console.log('response = ', response);
                this.setError([{text:response.data.error}]);
                return;
            }else{
                if(response.data.is_uniq===false){
                    this.temp_name = this.el_name.value;
                    this.is_name_uniq = false;
                    this.setError([{text:'Пользователь с таким именем уже есть'}]);
                    return;
                }
                //console.log('response = ', response);
                this.is_name_uniq = true;
                this.temp_name = this.el_name.value;
                this.delError();
            }
        })
        .catch((error)=>{
            console.error('error = ', error);
        });
    }

    onSave(event){
        if(this.el_name.value.length<3){
            this.setError([{text:"Логин не может быть меньше 3 символов"}]);
            //console.log("Логин не может быть меньше 3 символов");
            return;
        }

        if(!this.is_name_uniq){
            this.setError([{text:"Введите уникальный Логин"}]);
            //console.log("Логин не может быть меньше 3 символов");
            return;
        }

        if(this.el_password.value.length<3){
            this.setError([{text:"Пароль не может быть меньше 3 символов"}]);
            return;
        }

        if(this.el_role.value==='Выберите...'){
            this.setError([{text:"Выберите Роль пользователя"}]);
            return;
        }

        let arr_comp_sklad = [];

        if(this.el_role.value==3){
            const coll_selets = this.content.querySelectorAll('select[name$="sklad_comp"]');
            //console.log('coll_selets!!! = ', coll_selets);
            for (let select of coll_selets) {
                if(select.value!=='Выберите Склад(Компанию)'){
                    arr_comp_sklad.push(Number(select.value));
                }
            }

            if(arr_comp_sklad.length<=0){
                this.setError([{text:'Оператор должен иметь хотябы один Склад(компании)'}]);
                return;
            }
        }

        $('#form-add-user').modal('hide');
        let main = document.getElementById('user-table');
        main.dispatchEvent(new CustomEvent("addRow", {
            detail: {
                name: String(this.el_name.value),
                password: String(this.el_password.value),
                role: Number(this.el_role.value),
                arr_comp_sklad: arr_comp_sklad
            }
        }));
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new FormAddUser();