class FormEditKultur{
    constructor(){
        this.main = document.getElementById('form-edit-kultur');
        this.content = this.main.querySelector('div[class="modal-body"]');
        this.el_name = this.content.querySelector('input[name="name"]');
        this.el_old_class_kultur = this.content.querySelector('div[name="old_class_kultur"]');
        this.el_error = this.content.querySelector('div[name="error"]');
        
        this.templete_select_class_kultur = null;
        this.div_add_select = this.content.querySelector('div[name="div_add_select"]');

        this.arr_class_kultur = null;
        this.arr_el_select = [];
        this.counter = 0;
        this.temp_name = '';
        this.old_name = '';
        this.is_name_uniq = true;
        this.id = null;
        this.id_role = null;
        //console.log('создаю все события"!!!!');
        this.el_name.addEventListener('blur', this.isNameUniq.bind(this));
        this.div_add_select.firstElementChild.addEventListener('click', this.addSelectCompSklad.bind(this));
        this.main.addEventListener('render', this.render.bind(this));
        this.main.querySelector('button[name=save]').addEventListener('click', this.onSave.bind(this));
        //this.getTempete();
    }

    setTemplete(arr_class_kultur){
        if(this.templete_select_class_kultur){
            let select = this.templete_select_class_kultur.querySelector('select');
            select.innerHTML = '';
            arr_class_kultur.forEach(el=>{
                let newOption = new Option(`${el.name}`, el.id);
                select.append(newOption);
            });
            return;
        }
        let div_class_kultur = this.content.querySelector('div[name="class_kultur"]');
        arr_class_kultur.forEach(el=>{
            let newOption = new Option(`${el.name}`, el.id);
            div_class_kultur.querySelector('select').append(newOption);
        });

        this.templete_select_class_kultur = div_class_kultur.cloneNode(true);

        div_class_kultur.remove();
    }

    renderOldSkladComp(data){
        if(data.length<=0){
            this.el_old_class_kultur.style.display = 'none';
            return;
        }
        //console.log("el_old_class_kultur = ", this.el_old_class_kultur);
        this.el_old_class_kultur.style.display = '';
        let str = '';
        data.forEach(el=>{
            //console.log('sklad_comp = ', sklad_comp);
            str+=`<span class="badge badge-pill badge-info" name="${el.id_kult_class_kult}">${el.name_class_kultur}</span>`;
        });
        this.el_old_class_kultur.firstElementChild.innerHTML = str;
    }

    render(event){
        //console.log(event.detail.old_class_kultur);
        //console.log(event.detail.arr_class_kultur);
        //return;
        
        this.id = event.detail.id;
        this.el_name.value = event.detail.name_kultur;
        this.old_name=this.el_name.value
        this.delError();
        let arr_class_kultur = this.filtrArrSelect(event.detail.old_class_kultur, event.detail.arr_class_kultur);
        //return;
        this.setTemplete(arr_class_kultur);
        this.dellSelectAll();
        //this.clearSelect();
        //if(event.detail.role.id==3){
        this.renderOldSkladComp(event.detail.old_class_kultur);
        //}
        //console.log('event.detail.id_role = ', this.id_role);
        
        this.showBtnAddSelect(true);
        //console.log('должон открыть форму адд User');
        $('#form-edit-kultur').modal('show');
    }

    filtrArrSelect(old_arr,new_arr){
        //console.log(old_arr[0].id_class_kultur==new_arr[1].id);
        //return;
        let new_new_arr = [];
        new_arr.forEach(el=>{
            new_new_arr.push(el);
        });
        for(let i=0;i<new_new_arr.length;i++){
            for(let j=0;j<old_arr.length;j++){
                if(old_arr[j].id_class_kultur===new_new_arr[i].id){
                    if(new_new_arr[i].id!==1){
                        new_new_arr.splice(i,1);
                        i--;
                    }
                    break;
                }
            }
        }
        //console.log("new_arr = ", new_arr);
        return new_new_arr;
    }

    setError(errors){
        let text = '';
        errors.forEach(error=>{
            text+=error.text+'<br>';
        });
        this.el_error.firstElementChild.innerHTML = text;
        this.el_error.style.display = '';
    }

    delError(){
        this.el_error.style.display = 'none';
    }

    createSelect(){
        this.counter++;
        let div_select = this.templete_select_class_kultur.cloneNode(true);
        let select = div_select.querySelector('select');
        select.setAt

        div_select.querySelector('button').addEventListener('click', this.dellSelect.bind(this));
        select.addEventListener('change', this.selectSklad.bind(this));
        this.arr_el_select.push(div_select);
        this.content.append(div_select);
    }

    showBtnAddSelect(show){
        if(show){
            this.div_add_select.style.display = '';
        }else{
            this.div_add_select.style.display = 'none';
        }
    }

    addSelectCompSklad(){
        let collect_div_select = this.content.querySelectorAll('select[name="class_kultur"]');
        for (let select of collect_div_select) {
            if(Number(select.value)===1){
                return;
            }
        }
        this.createSelect();
    }

    dellSelectAll(){
        //console.log('должен удалить все селекты');
        let collect_div_select = this.content.querySelectorAll('select[name="class_kultur"]');
        for (let select of collect_div_select) {
            select.parentElement.remove();
        }
    }

    dellSelect(event){
        event.target.parentElement.parentElement.remove();
        //console.log('Жмакнул удалить селект');
    }

    selectSklad(event){
        const coll_selets = this.content.querySelectorAll('select[name="class_kultur"]');
        
        //console.log('coll_selets!!! = ', coll_selets);
        for (let select of coll_selets) {
            if(select!==event.target){
                //console.log('select.value = ', select.value,'//',event.target.value);
                if(select.value===event.target.value){
                    event.target.value = '1';
                    return;
                }
            }
        }
    }

    isNameUniq(event){
        //console.log('проверяю!!!');
        if(this.old_name===this.el_name.value){
            return;
        }

        if(this.el_name.value.length<2){
            this.setError([{text:'Слишком короткий Название'}]);
            return;
        }

        if(this.temp_name===this.el_name.value){
            return;
        }

        this.delError();

        axios({
            method: 'get',
            url: '/user_admin/KulturController.php',
            params:{
                is_name_uniq: this.el_name.value
            }
        })
        .then((response)=>{
            if('error' in response.data){
                //console.log('response = ', response);
                this.setError([{text:response.data.error}]);
                return;
            }else{
                if(response.data.is_uniq===false){
                    this.temp_name = this.el_name.value;
                    this.is_name_uniq = false;
                    this.setError([{text:'Культура с таким именем уже есть'}]);
                    return;
                }
                //console.log('response = ', response);
                this.is_name_uniq = true;
                this.temp_name = this.el_name.value;
                this.delError();
            }
        })
        .catch((error)=>{
            console.error('error = ', error);
        });
    }

    onSave(event){
        if(this.el_name.value.length<2){
            this.setError([{text:"Логин не может быть меньше 3 символов"}]);
            //console.log("Логин не может быть меньше 3 символов");
            return;
        }

        if(!this.is_name_uniq){
            this.setError([{text:"Введите уникальный Название Культуры"}]);
            //console.log("Логин не может быть меньше 3 символов");
            return;
        }

        let arr_class_kultur = [];
        const coll_selets = this.content.querySelectorAll('select[name="class_kultur"]');
        //console.log('coll_selets!!! = ', coll_selets);
        for (let select of coll_selets) {
            if(Number(select.value)!==1){
                arr_class_kultur.push(Number(select.value));
            }
        }

        $('#form-edit-kultur').modal('hide');
        let main = document.getElementById('kultur-table');
        main.dispatchEvent(new CustomEvent("updateRow", {
            detail: {
                id: this.id,
                name_kultur: String(this.el_name.value),
                arr_class_kultur: arr_class_kultur
            }
        }));
    }
}

// window.onload = function() {
//     new FormEditCompany();
// };
new FormEditKultur();