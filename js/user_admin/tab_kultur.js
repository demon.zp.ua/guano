class TablKultur{
    constructor(){
        this.main = document.getElementById('kultur-table');
        this.btn_add = document.getElementById('add_kultur');
        this.btn_previous = document.getElementById('component-kultur').querySelector('button[name="previous"]');
        this.btn_next = document.getElementById('component-kultur').querySelector('button[name="next"]');

        this.btn_previous.addEventListener('click', this.previousPage.bind(this));
        this.btn_next.addEventListener('click',this.nextPage.bind(this));
        this.btn_add.addEventListener('click', this.onClickAdd.bind(this));
        this.main.addEventListener('render', this.loadData.bind(this));
        this.main.addEventListener('delete', this.delete.bind(this));
        this.main.addEventListener('updateRow', this.onUpdate.bind(this));
        this.main.addEventListener('addRow', this.addRow.bind(this));

        this.el_templete_row = null;
        this.fon_load = null;
        this.data_table = {};
        this.arr_comp = null;
        this.arr_class_kultur = [];
        this.page = 0;
        this.num_rows = 20;
        this.has_page = false;
        this.id_del = null;
        this.getTempete();
    }

    getTempete(){
        this.fon_load = document.createElement('div');
        this.fon_load.style.position = 'absolute';
        this.fon_load.style.top = '0px';
        this.fon_load.style.width= window.innerWidth+'px';
        this.fon_load.style.height=document.documentElement.scrollHeight+'px';
        this.fon_load.style.margin= '0 auto';
        //this.fon_load.style.overflow= "hidden";
        this.fon_load.style.backgroundColor = '#000000';
        this.fon_load.style.opacity = 0.5;

        let el_t_row = this.main.querySelector('tbody').querySelector('tr');
        this.el_templete_row = el_t_row.cloneNode(true);
        //console.log('el_t_row = ', this.el_templete_row);
        el_t_row.remove();
    }

    createRow(row){
        this.data_table[row.id] = row;
        let el_temp_row = this.el_templete_row.cloneNode(true);
        el_temp_row.setAttribute('name', row.id);
        //console.log('row = ', el_temp_row);
        for(let column in row){
            if(el_temp_row.querySelector(`th[name="${column}"]`)){
                let el_th = el_temp_row.querySelector(`th[name="${column}"]`);
                if(column!=='arr_class_kultur'){
                    el_th.innerText = row[column];
                }else{
                    el_th.innerHTML = this.getSpans(row[column]);
                }
            }
            //console.log('el_th = ', el_th);
        }
        let btn_edit = el_temp_row.querySelector(`button[name="edit"]`);
        btn_edit.addEventListener('click', this.onClickEdit.bind(this));

        this.main.querySelector('tbody').append(el_temp_row);

    }

    getSpans(arr){
        let str = '';
        arr.forEach(el=>{
            str+=`<span class="badge badge-pill badge-info" name="${el.id_class_kultur}">${el.name_class_kultur}</span>`;
        });
        //console.log('str = ', str);
        return str;
    }

    loadData(){
        //console.log('должен послать запрос');
        this.loadDataClassCultur();
        axios({
            method: 'get',
            url: '/user_admin/KulturController.php',
            params:{
                page: this.page,
                num_rows: this.num_rows
            }
        })
        .then((response)=>{
            console.log('response = ', response);
            if('error' in response.data){
                console.log('response = ', response.data.error);
                //this.addError([{text:response.data.error}]);
                return;
            }
            // for(let i=0;i<response.data.success.data.length-1;i++){
            //     this.createRow(response.data.success.data[i]);
            // }
            response.data.success.data.forEach((element,i) => {
                if(i<this.num_rows){
                    this.createRow(element);
                }
            });
            if(this.num_rows<response.data.success.num_rows){
                this.has_page = true;
                this.showBtnNext(true);
            }else{
                this.has_page = false;
                this.showBtnNext(false);
            }
            // for(let row in response.data.success.data){
                
            // }
            
        })
        .catch((error)=>{
            console.log('error = ', error);
        });
    }

    loadDataClassCultur(){
        axios({
            method: 'get',
            url: '/user_admin/ClassKulturController.php',
            params:{
                page: 0,
                num_rows: 1000
            }
        })
        .then((response)=>{
            console.log('response class = ', response);
            if('error' in response.data){
                console.log('response class = ', response.data.error);
                //this.addError([{text:response.data.error}]);
                return;
            }
            this.arr_class_kultur = response.data.success.data;
        })
        .catch((error)=>{
            console.log('error = ', error);
        });
    }

    onClickAdd(){
        //console.log('nen!!!');
        document.getElementById('form-add-kultur').dispatchEvent(new CustomEvent("render",{
            detail: {
                arr_class_kultur:this.arr_class_kultur
            }
        }));
    }

    onClickEdit(event){
        //console.log('ТУТУ!!!!!!!');
        let id = event.target.parentElement.parentElement.getAttribute('name');
        console.log(document.getElementById('form-edit-kultur'));
        document.getElementById('form-edit-kultur').dispatchEvent(new CustomEvent("render", {
            detail: { 
                id: this.data_table[id].id,
                name_kultur: this.data_table[id].name_kultur,
                old_class_kultur: this.data_table[id].arr_class_kultur,
                arr_class_kultur:this.arr_class_kultur
            }
        }));
        
        //console.log('должен редактировать = ', event.target.parentElement.parentElement.getAttribute('name'));
    }

    addRow(event){
        const data = event.detail;
        //console.log(data);
        //return;
        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/KulturController.php',
            data:{
                add_kultur:data
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            //console.log('response = ', response);
            if('error' in response.data){
                //console.log('response = ', response);
                this.fon_load.remove();
                return;
            }
            //return;
            this.fon_load.remove();
            this.delete();
            this.loadData();
            alert(`Культура "${data.name}" успешно создан`);
            return;
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
        });
    }

    onUpdate(event){
        const data = event.detail;
         
        if(String(data.id).length<=1 && String(data.name).length<=2){
            return;
        }

        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/KulturController.php',
            data:{
                edit_kultur: data
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            console.log('response = ', response);
            if('error' in response.data){
                console.log('Error = ',response.data.error);
                alert('Error = ', response.data.error);
                return;
            }
            //return;
            //this.updateRow(data);
            alert(`Культура "${data.name}" успешно Изменена`);
            this.fon_load.remove();
            this.delete();
            this.loadData();
            
            //document.body.setAttribute('class','');
            //resolve(response.data);
            return;
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //reject(error);
        });
    }

    previousPage(){
        if(this.page<=0){
            return;
        }
        this.page--;
        this.delete();
        this.loadData();
    }

    nextPage(){
        if(!this.has_page){
            return;
        }
        this.page++;
        this.delete();
        this.loadData();
    }

    showBtnNext(show){
        if(show){
            this.btn_next.style.display = '';
        }else{
            this.btn_next.style.display = 'none';
        }
    }

    delete(){
        this.data_table = {};
        //console.log(this.main.querySelector('tbody').rows);
        for(let elem of this.main.querySelector('tbody').querySelectorAll('tr')){
            elem.remove();
        }
    }
}

new TablKultur();