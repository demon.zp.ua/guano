class TabUser{
    constructor(){
        //console.log('я тут!!!!');
        this.main = document.getElementById('user-table');
        this.btn_add = document.getElementById('add_user');
        this.btn_previous = document.getElementById('component-users').querySelector('button[name$="previous"]');
        this.btn_next = document.getElementById('component-users').querySelector('button[name$="next"]');

        this.btn_previous.addEventListener('click', this.previousPage.bind(this));
        this.btn_next.addEventListener('click',this.nextPage.bind(this));
        this.btn_add.addEventListener('click', this.onClickAdd.bind(this));
        this.main.addEventListener('render', this.loadData.bind(this));
        this.main.addEventListener('delete', this.delete.bind(this));
        this.main.addEventListener('updateRow', this.onUpdate.bind(this));
        this.main.addEventListener('addRow', this.addRow.bind(this));

        this.el_templete_row = null;
        //this.el_span = null;
        this.fon_load = null;
        this.data_table = {};
        this.arr_sklad_comp = [];
        this.page = 0;
        this.num_rows = 20;
        this.has_page = false;
        this.id_del = null;
        this.getTempete();
        //document.body.append(this.fon_load);
    }

    getTempete(){
        this.fon_load = document.createElement('div');
        this.fon_load.style.position = 'absolute';
        this.fon_load.style.top = '0px';
        this.fon_load.style.width= window.innerWidth+'px';
        this.fon_load.style.height=document.documentElement.scrollHeight+'px';
        this.fon_load.style.margin= '0 auto';
        //this.fon_load.style.overflow= "hidden";
        this.fon_load.style.backgroundColor = '#000000';
        this.fon_load.style.opacity = 0.5;

        // let el_t_span = this.main.querySelector('tbody').querySelector('span');
        // this.el_span = el_t_span.cloneNode(true);

        // el_t_span.remove();

        let el_t_row = this.main.querySelector('tbody').querySelector('tr');
        this.el_templete_row = el_t_row.cloneNode(true);
        //console.log('el_t_row = ', this.el_templete_row);
        el_t_row.remove();
    }

    createRow(row){
        this.data_table[row.id] = row;
        let el_temp_row = this.el_templete_row.cloneNode(true);
        el_temp_row.setAttribute('name', row.id);
        //console.log('row = ', el_temp_row);
        for(let column in row){
            if(el_temp_row.querySelector(`th[name$="${column}"]`)){
                let el_th = el_temp_row.querySelector(`th[name$="${column}"]`);
                if(column!=='sklad_comp'){
                    let val = row[column];
                    if(column==='is_activ'){
                        if(row[column]==1){
                            val = 'Да';
                        }else{
                            val = 'Нет';
                        }
                    }
                    el_th.innerText = val;
                }else{
                    el_th.innerHTML = this.getSpans(row[column]);
                }
            }
            //console.log('el_th = ', el_th);
        }

        let btn_edit = el_temp_row.querySelector(`button[name$="edit"]`);
        btn_edit.addEventListener('click', this.onClickEdit.bind(this));

        this.main.querySelector('tbody').append(el_temp_row);

    }

    getSpans(arr_comp_sklad){
        let str = '';
        for(let i=0;i<arr_comp_sklad.length;i++){
            if(i<3){
                str+=`<span class="badge badge-pill badge-info" name="${arr_comp_sklad[i].name_sklad+arr_comp_sklad[i].name_comp}">${arr_comp_sklad[i].name_sklad}(${arr_comp_sklad[i].name_comp})</span>`;
            }
        }
        if(arr_comp_sklad.length>3){
            str+=`<span class="badge badge-pill badge-info" name="...">...</span>`;
        }
        //console.log('str = ', str);
        return str;
    }

    updateRow(data,dop_msg){
        let str_del_sklad = '';
        let str_not_del_sklad = '';
        if(dop_msg){
            //console.log('пытаюсь удалить');
            for(let i=0;i<dop_msg.del_sklads.length;i++){
                for(let j=0;j<this.data_table[data.id]['sklad_comp'].length;j++){
                    if(this.data_table[data.id]['sklad_comp'][j].id===dop_msg.del_sklads[i]){
                        str_del_sklad+=`"${this.data_table[data.id]['sklad_comp'][j].name_sklad}(${this.data_table[data.id]['sklad_comp'][j].name_comp})" `;
                        this.data_table[data.id]['sklad_comp'].splice(j,1);
                        break;
                    }
                }
            }
        }
        
        //const data = event.detail;
        
        for(let i=0;i<data.arr_comp_sklad.length;i++){
            for(let j=0;j<this.arr_sklad_comp.length;j++){
                //console.log('id = ', data.arr_comp_sklad[i], 'id2 = ', this.arr_sklad_comp[j].id);
                if(data.arr_comp_sklad[i]===this.arr_sklad_comp[j].id){
                    data.arr_comp_sklad[i] = {
                        'id': this.arr_sklad_comp[j].id, 
                        'name_comp': this.arr_sklad_comp[j].name_comp,
                        'name_sklad': this.arr_sklad_comp[j].name_sklad
                    };
                    this.arr_sklad_comp.splice(j,1);
                    break;
                }
            }
        }
        //console.log('this.data_table[data.id] = ', this.data_table[data.id]);
        //console.log('data.arr_comp_sklad = ', data.arr_comp_sklad);
        //this.data_table[data.id] = data;
        for(let key in data){
            if(key==='arr_comp_sklad'){
                data[key].forEach(sklad_comp=>{
                    this.data_table[data.id]['sklad_comp'].push({'id':sklad_comp.id,'name_comp':sklad_comp.name_comp,'name_sklad':sklad_comp.name_sklad});
                });
                //this.data_table[data.id]['sklad_comp']
            }
            //this.data_table[data.id][key] = data[key];

        }
        //console.log('this.data_table[data.id] = ', this.data_table[data.id]);
        let el_temp_row = this.main.querySelector('tbody').querySelector(`tr[name="${data.id}"]`);
        el_temp_row.querySelector('th[name="name_user"]').innerText = data.name_user;
        let val_is_activ = 'Да';
        if(Number(data.is_activ)===0){
            val_is_activ = 'Нет';
        }
        this.data_table[data.id].is_activ = val_is_activ;
        el_temp_row.querySelector('th[name="is_activ"]').innerText = val_is_activ;

        // if(data.arr_comp_sklad.length<=0 || this.data_table[data.id]['sklad_comp'].length>4){
        //     return;
        // }

        const el_sklad_comp = el_temp_row.querySelector('th[name="sklad_comp"]');
        el_sklad_comp.innerHTML = '';
        //return;
        //let i = this.data_table[data.id]['sklad_comp'].length;
        let i = 1;
        this.data_table[data.id].sklad_comp.forEach(sklad_comp=>{
            let span = null;    
            if(i<=3){
                span = document.createElement('span');
                span.setAttribute('class', 'badge badge-pill badge-info');
                span.setAttribute('name',`${sklad_comp.name_sklad+sklad_comp.name_comp}`);
                span.innerText = `${sklad_comp.name_sklad}(${sklad_comp.name_comp})`;
            }
            if(i===4){
                span = document.createElement('span');
                span.setAttribute('class', 'badge badge-pill badge-info');
                span.setAttribute('class', 'badge badge-pill badge-info');
                span.setAttribute('name','...');
                span.innerText = '...';
            }
            str_not_del_sklad+=`"${sklad_comp.name_sklad}(${sklad_comp.name_comp})" `;
            i++;
            if(span){
                el_sklad_comp.append(span);
            }
            //console.log('должен добавить = ',span);   
        });

        if(dop_msg){
            alert("удалось отвязать склады: "+str_del_sklad+"Не удалось отвязать склады: "+str_not_del_sklad+"возможно склад(ы) связан(ы) с другими данными.");
        }
        
        //span.setAttribute('name',`${arr_comp_sklad[i].name_sklad+arr_comp_sklad[i].name_comp}`);
        //console.log("data = ", String(data.id).length);
        // if(String(data.id).length>=1 && String(data.name).length>=2){
        //     this.onSave(data);
        // }
    }

    addRow(event){
        const data = event.detail;
        //console.log(data);
        //return;
        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/UserController.php',
            data:{
                add_user:data
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            //console.log('response = ', response);
            if(response.data.hasOwnProperty('error')){
                console.log('response = ', response.data.error);
                this.fon_load.remove();
                return;
            }

            if(!response.data.hasOwnProperty('success')){
                this.fon_load.remove();
                console.log('ERROR = ', response);
                return;
            }
            //return;
            this.fon_load.remove();
            this.delete();
            this.loadData();
            alert(`Пользователь "${data.name}" успешно создан`);
            //document.body.setAttribute('class','');
            //resolve(response.data);
            return;
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //reject(error);
        });
    }

    loadData(){
        //console.log('должен послать запрос');
        //console.log('axios = ', axios);
        this.loadDataCompSklad();
        axios({
            method: 'get',
            url: '/user_admin/UserController.php',
            params:{
                page: this.page,
                num_rows: this.num_rows
            }
        })
        .then((response)=>{
            //console.log('response = ', response);
            if(response.data.hasOwnProperty('error')){
                console.log('response = ', response.data.error);
                //this.fon_load.remove();
                return;
            }

            if(!response.data.hasOwnProperty('success')){
                //this.fon_load.remove();
                console.log('ERROR = ', response);
                return;
            }
            response.data.success.data.forEach((element,i) => {
                if(i<this.num_rows){
                    this.createRow(element);
                }
            });
            if(this.num_rows<response.data.success.num_rows){
                this.has_page = true;
                this.showBtnNext(true);
            }else{
                this.has_page = false;
                this.showBtnNext(false);
            }
            // for(let row in response.data.success.data){
                
            // }
            
        })
        .catch((error)=>{
            console.log('error = ', error);
        });
    }

    loadDataCompSklad(){
        axios({
            method: 'get',
            url: '/user_admin/UserController.php',
            params:{
                get_comp_sklad:'get_comp_sklad'
            }
        })
        .then((response)=>{
            //console.log('response = ', response);
            if(response.data.hasOwnProperty('error')){
                console.log('response = ', response.data.error);
                //this.fon_load.remove();
                return;
            }

            if(!response.data.hasOwnProperty('success')){
                //this.fon_load.remove();
                console.log('ERROR = ', response);
                return;
            }

            this.arr_sklad_comp = response.data.success.data;

        })
        .catch((error)=>{
            console.log('error = ', error);
        });
    }

    previousPage(){
        if(this.page<=0){
            return;
        }
        this.page--;
        this.delete();
        this.loadData();
    }

    nextPage(){
        if(!this.has_page){
            return;
        }
        this.page++;
        this.delete();
        this.loadData();
    }

    showBtnNext(show){
        if(show){
            this.btn_next.style.display = '';
        }else{
            this.btn_next.style.display = 'none';
        }
    }

    delete(){
        this.data_table = {};
        //console.log(this.main.querySelector('tbody').rows);
        for(let elem of this.main.querySelector('tbody').querySelectorAll('tr')){
            elem.remove();
        }
    }

    onClickEdit(event){
        //let e = new Event("render", {bubbles: true});
        // document.getElementById('form-edit-company').dispatchEvent(new CustomEvent("render", {
        //     detail: { 
        //         id: event.target.parentElement.parentElement.querySelector('th[name$=id]').innerText,
        //         name: event.target.parentElement.parentElement.querySelector('th[name$=name]').innerText
        //     }
        // }));
        let id = event.target.parentElement.parentElement.getAttribute('name');
        document.getElementById('form-edit-user').dispatchEvent(new CustomEvent("render", {
            detail: { 
                id: this.data_table[id].id,
                id_role: this.data_table[id].id_role,
                name_user: this.data_table[id].name_user,
                is_activ: this.data_table[id].is_activ,
                name_role: this.data_table[id].name_role,
                old_sklad_comp: this.data_table[id].sklad_comp,
                arr_sklad_comp:this.arr_sklad_comp
            }
        }));
        
        //console.log('должен редактировать = ', event.target.parentElement.parentElement.getAttribute('name'));
    }

    onClickAdd(){
        document.getElementById('form-add-user').dispatchEvent(new CustomEvent("render",{
            detail: {
                arr_sklad_comp:this.arr_sklad_comp
            }
        }));
    }

    onUpdate(event){
        const data = event.detail;
        data['id_role'] = this.data_table[data.id].id_role;
        data['id_user_role'] = this.data_table[data.id].id_user_role; 
        if(String(data.id).length<=1 && String(data.name).length<=2){
            return;
        }

        document.body.append(this.fon_load);
        axios({
            method: 'post',
            url: '/user_admin/UserController.php',
            data:{
                edit_user: data
            }
        })
        //обработака ответа запроса все что со статусом 200
        .then((response)=>{
            //console.log('response = ', response);
            if(response.data.hasOwnProperty('error')){
                console.log('Error = ',response.data.error);
                alert('Error = ', response.data.error);
                this.fon_load.remove();
                return;
            }

            if(!response.data.hasOwnProperty('success')){
                console.log('Error = ',response);
                this.fon_load.remove();
                return;
            }
            //this.fon_load.remove();
            //return;
            let dop_msg = null;
            if(response.data.success.hasOwnProperty('dop_msg')){
                dop_msg = response.data.success.dop_msg;
            }
            this.updateRow(data,dop_msg);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //resolve(response.data);
            return;
        })
        //обработака ответа запроса с ошибкой не помню какие статусы
        .catch((error)=>{
            console.log('error = ', error);
            this.fon_load.remove();
            //document.body.setAttribute('class','');
            //reject(error);
        });
    }
}

new TabUser();