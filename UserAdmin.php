<?php
    require_once './user_admin/views/win_confirm.php';
    require_once './user_admin/views/tabl_users.php';
    require_once './user_admin/views/tabl_companys.php';
    require_once './user_admin/views/tabl_sklads.php';
    require_once './user_admin/views/tabl_class_kulturs.php';
    require_once './user_admin/views/tabl_kultur.php';
    //require_once './user_admin/userController.php';
    //require_once './user_admin/CompanyController.php';
    
    include 'Connect.php';

	session_start();

	//$dbc = mysqli_connect('localhost', 'adminko', 'S2y2D8a2', 'agroholding_rest');
  
	//$dbc = mysqli_connect('localhost', 'root', '', 'agroholding_rest');
	
	if (mysqli_connect_errno()) 
	{
    	printf("Connect failed: %s\n", mysqli_connect_error());
    	exit();
    }
    
    $fgh = 0;

	if($_SESSION['user_role'] <> 1)
	{
		header('Location: '. 'Exit.php');
    }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Админка &middot; Агро статистика</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="form">
<form class="form-horizontal" id="admin-form" role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">    
	<nav class="navbar navbar-default navbar-light bg-light justify-content-end">
		<div class="container-fluid">
			<div class="navbar-header">
				АгроСтатистик
			</div>
			<!--
			<div>
				<ul>
					<a class="nav-link" href="UserAdminRegistr.php">Регистрация</a>
				</ul>
			</div>
			-->
			<div>
				<ul>           
		<a class="nav-link" href="Exit.php">Выйти     	
<?php 
	// Если посетитель "вошёл" - приветствуем его 
	if (isset($_SESSION['user_name']))
	{
 	//echo "(".$_SESSION['user'].")"; 
	echo '('.$_SESSION['user_name'].')';
	}
?>	        	
					</a>		
            	</ul>
			</div>
		</div>
    <!--
    	<div class="collapse navbar-collapse flex-grow-0" id="navbarContent">
        	<ul class="navbar-nav text-right">
            	<li class="nav-item active">
                	<a class="nav-link" href="Logins.php">Войти</a>
            	</li>
        	</ul>
    	</div>
    	-->
	</nav>
    <h4>Управление справочниками</h4>
 
<!--
ссылки как кнопки для выбора справочника    	
-->
<?php

getWinConfirm();
    echo <<<EOD
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#users" id="tab-user">Пользователи</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu1" id="tab-comp">Компании</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu2" id="tab-sklad">Склады</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#class_kultura" id="tab-class-kultur">Классы Культур</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#kulturs" id="tab-kultur">Культуры</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane container active" id="users">
EOD;
        getUserTable();
    echo <<<EOD
    </div>
    <div class="tab-pane container fade" id="menu1">
EOD;
        getCompanyTable();
    echo <<<EOD
    </div>
    <div class="tab-pane container fade" id="menu2">
EOD;
        getSkladTable();
    echo <<<EOD
    </div>
    <div class="tab-pane container fade" id="class_kultura">
EOD;
        getClassKulturTable();
    echo <<<EOD
    </div>
    <div class="tab-pane container fade" id="kulturs">
EOD;
        getKulturTable();
    echo<<<EOD
    </div>
</div>
EOD;
?>

</form>
</div>
    <!-- на jQuery (необходим для Bootstrap - х JavaScript плагины)-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
    </script>
    
    <!-- Включают все скомпилированные плагины (ниже), или включать отдельные файлы по мере необходимости  -->
    <script src="js/bootstrap.min.js">
    </script>
    <script src="js/axios.min.js"></script>
    <!-- <script src="js/user_admin/user_admin.js"></script> -->
    <script src="js/user_admin/app.js"></script>
  </body>
</html>